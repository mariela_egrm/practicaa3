/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mipaquete;

import java.util.Scanner;

/**
 *
 * @author Mariela Esther Gomez Rivera C.I. 6680592
 */
public class COLA3 {
    public static void main(String[] args) {
        mainCOLA c;
        c = new mainCOLA(5);
        
        Scanner x=new Scanner(System.in);
        System.out.println("++++ Menu de colas ++++");
        
        System.out.println(" - Añadir un nuevo elemento");
        System.out.println(" - Borrar un elemento de la estructura cola");
        System.out.println(" - Mostrar el numero de elementos que existe en la estructura cola");
        System.out.println(" - Mostrar el elemento minimo y maximo");
        System.out.println(" - Buscar un elemento dentro de la estructura cola");
        System.out.println(" - Mostrar todos los elementos");
        System.out.println(" - Salir");
        
        int elegir;
        int cont=0;
        while(cont==0){
            System.out.println("elija una opcion");
            elegir =x.nextInt();
            switch(elegir){
                case 1:
                    
                    System.out.println("agregar un valor a la cola");
                    int valor=x.nextInt();
                    c.push(valor);
                    break;
                    
                case 2:
                    System.out.println("se borro: " + c.pop());
                    break;
                case 3:
                    System.out.println("estan: " + c.tamaño()+ " elementos en la cola");
                    break;
                case 4:
                    break;
                case 5:
                    break;
                case 6:
                    System.out.println("Los elementos de la cola son:");
                    int tam=c.tamaño();
                    while(tam>=1){
                        System.out.println(c.pop());
                        tam--;   
                    }   
                    break;
                case 7:
                    System.out.println("saliste");
                    break;
                default:
                    System.out.println("opcion no valida");
                    break;
            }
        }
        
    }
    
}
